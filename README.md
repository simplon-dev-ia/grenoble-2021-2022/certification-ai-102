# Préparation à l'AI-102

<img src="https://training.cellenza.com/wp-content/uploads/2021/08/AI102-1.png" alt="drawing" width="200"/>

- **Durée de l'examen :** 130 minutes

- **Nombre de questions :** de 40 à 60 questions 

- **Durée de validité de la certification :** 1 an + renouvellement possible pour 1 an supplémentaire avec questionnaire non surveillé

## Ressources

Contenu et thèmatiques de la certification AI-102 : https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RE4MbYD

Modules de Microsoft Learn :

- [Prepare for AI engineering](https://docs.microsoft.com/en-us/learn/paths/prepare-for-ai-engineering/)

- [Provision and manage Azure Cognitive Services](https://docs.microsoft.com/en-us/learn/paths/provision-manage-azure-cognitive-services/)

- [Process and translate text with Azure Cognitive Services](https://docs.microsoft.com/en-us/learn/paths/process-translate-text-azure-cognitive-services/)

- [Process and Translate Speech with Azure Cognitive Speech Services](https://docs.microsoft.com/en-us/learn/paths/process-translate-speech-azure-cognitive-speech-services/)

- [Create a Language Understanding solution](https://docs.microsoft.com/en-us/learn/paths/create-language-understanding-solution/)

- [Build a question answering solution](https://docs.microsoft.com/en-us/learn/paths/build-qna-solution/)

- [Create conversational AI solutions](https://docs.microsoft.com/en-us/learn/paths/create-conversational-ai-solutions/)

- [Create computer vision solutions with Azure Cognitive Services](https://docs.microsoft.com/en-us/learn/paths/create-computer-vision-solutions-azure-cognitive-services/)

- [Extract text from images and documents](https://docs.microsoft.com/en-us/learn/paths/extract-text-from-images-documents/)

- [Implement knowledge mining with Azure Cognitive Search](https://docs.microsoft.com/en-us/learn/paths/implement-knowledge-mining-azure-cognitive-search/)

## Quiz de préparation

- Quiz N°1 : https://forms.gle/m8pLXQ2u5YavaLkBA

- Quiz N°2 : https://forms.gle/N3drVdA2vqjyrjMu8

Sources pour les quiz :

- Examtopics : https://www.examtopics.com/exams/microsoft/ai-102/

## Planning de passage des certifications AI-102

Les certifications AI-102 sont passées durant la semaine du 30 mai au 3 juin 2022

Lien vers le planning de passage : https://docs.google.com/spreadsheets/d/1i9-Hqj5Ev1GanJ7CcO3hZWMXZRLeJbal-T53sKR6_mQ

## Liens pour les labs et ressources complémentaires pour la formation Microsoft

Lien vers les labs : https://microsoftlearning.github.io/AI-102-AIEngineer/

- Module 1: Introduction to AI on Azure

**Ressources :**

https://www.microsoft.com/ai/responsible-ai

https://azure.microsoft.com/fr-fr/product-categories/applied-ai-services/

https://azure.microsoft.com/fr-fr/services/search/

- Module 2: Developing AI Apps with Cognitive Services

**Labs :**

https://microsoftlearning.github.io/AI-102-AIEngineer/Instructions/01-get-started-cognitive-services.html

https://microsoftlearning.github.io/AI-102-AIEngineer/Instructions/02-cognitive-services-security.html

https://microsoftlearning.github.io/AI-102-AIEngineer/Instructions/03-monitor-cognitive-services.html

https://microsoftlearning.github.io/AI-102-AIEngineer/Instructions/04-use-a-container.html

- Module 3: Getting Started with Natural Language Processing

- Module 4: Building Speech-Enabled Applications

- Module 5: Creating Language Understanding Solutions

- Module 6: Building a QnA Solution

- Module 7: Conversational AI and the Azure Bot Service

- Module 8: Getting Started with Computer Vision

- Module 9: Developing Custom Vision Solutions

- Module 10: Detecting, Analyzing, and Recognizing Faces

- Module 11: Reading Text in Images and Documents

- Module 12: Creating a Knowledge Mining Solution

